.. include:: macros.hrst

Welcome to ParaView Documentation !
===================================

This guide can be split into three volumes.
The **User's Guide** covers various aspects of data analysis and visualization with |ParaView|.

The **Reference Manual** provides details on various components in the UI and the scripting API.

The **Classroom Tutorials** provides beginning, advanced, python and batch, and targeted tutorial
lessons on how to use |ParaView|.

.. toctree::
   :maxdepth: 2
   :caption: ParaView User's Guide

   UsersGuide/index.rst
   
.. toctree::
   :maxdepth: 2
   :caption: ParaView Reference Manual

   ReferenceManual/index.rst

.. toctree::
   :maxdepth: 2
   :caption: ParaView Classroom Tutorials

   ClassroomTutorials/index.rst
   
.. toctree::
   :maxdepth: 1
   :caption: Appendix

   references

