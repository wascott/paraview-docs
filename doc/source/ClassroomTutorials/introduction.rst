.. include:: ../macros.hrst
.. include:: ../abbreviations.hrst

.. _chapter:TutorialsIntroduction:

Introduction to ParaView Classroom Tutorials
############################################

**Classroom Tutorials** contain beginning, advanced, python and batch, and targeted tutorial lessons on how to use
|ParaView|, created by Sandia National Laboratories. These **tutorials** were created by W. Alan Scott and are
presented as 3-hour classes internally within Sandia National Laboratories.

Training Data
=============
|ParaView| binaries are downloaded from here: https://www.paraview.org/download/.

|ParaView| data referenced in this tutorial are also located at the download site here: https://www.paraview.org/download/.

Acknowledgements
================

Sandia National Laboratories is a multi-mission laboratory managed and operated by National Technology and Engineering
Solutions of Sandia, LLC., a wholly owned subsidiary of Honeywell International, Inc., for the U.S. Department of
Energy’s National Nuclear Security Administration under contract DE-NA-0003525.
