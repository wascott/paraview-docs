.. include:: ../macros.hrst
.. include:: ../abbreviations.hrst

.. _chapter:PythonAndBatchPvpythonAndpvbatch:

Python & Batch: pvpython and pvbatch
####################################

Introduction
============

ParaView can run without opening the ParaView GUI, and is controlled
through Python. There are two Python interfaces - pvpython and pvbatch.

pvpython
========

|pvpython| is the Python interface to ParaView. You can think of |pvpython|
as ParaView with a Python interface. As we did with the Python Shell,
you can manually type in commands to |pvpython|. The first thing you will
want to do is import paraview simple, as follows:

.. code-block:: python

   from paraview.simple import

|pvpython| can also read Python command files. Type **pvpython --help** for
arguments. Running |pvpython| files looks like this:

.. code-block:: console

   /pathTopvpython/pvpython /pathToPythonCommandFile/commandFile.py

   # Example
   D:/alan/paraview/pvpython D:/alan/scripts/disk_out_ref-A.py

-  You will notice that |pvpython| will run the script and then exit. The
   output of the script is a screenshot or other data product.
-  Anywhere that needs editing in the scripts above will be marked by the string **editMeHere**.
-  You will need to hard code in the paths to your data, and paths for output products.
-  The first time you run a script with |pvpython|, the output will be a
   postage stamp sized window. You can change this by finding and
   uncommenting the line **renderView1.ViewSize**.

-  Try making and running a script of your own. Alternatively, here is
   an example. Cut and paste the following into a file named
   greenSphere.py:

   .. code-block:: python

      #!/usr/bin/env pvpython

      from paraview.simple import

      # Lets create a sphere
      sphere=Sphere()
      Show()
      Render()

      # get active view
      renderView1 = GetActiveViewOrCreate('RenderView')
      renderView1.ViewSize = [1670, 1091]

      # get display properties
      sphere1Display = GetDisplayProperties(sphere, view=renderView1)

      # change solid color
      sphere1Display.AmbientColor = [0.0, 1.0, 0.0]
      sphere1Display.DiffuseColor = [0.0, 1.0, 0.0]

      # save screenshot
      SaveScreenshot('greenSphereScreenshot.png', renderView1,
      ImageResolution=[1670, 1091])

-  Run this as follows:

   .. code-block:: console

      /pathTopvpython/pvpython greenSphere.py

pvbatch
========

|pvbatch| is like |pvpython|, with two exceptions. |pvbatch| only accepts
commands from input scripts, and |pvbatch| will run in parallel if it was
built using MPI. Input is exactly like |pvpython|.

Generic user specific section
-----------------------------

This section describes how to use pvbatch when on Windows PCs.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  If you are training on Linux, |pvbatch| will exist in the bin
   directory.
-  If you are training on OS X, open a terminal window, and cd to
   /Applications/ParaView x.x.x/Contents/bin. |pvbatch| will be
   located here.
-  If you are training on Windows, |pvbatch| does not exist. But, for a
   single process, such as this training, |pvpython| will substitute.

Lets create a Python trace.
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since we are on Windows, we will create a python trace, and use |pvpython|
to process it.

-  Read exodus data, screenshot, movie.

   -  Run |paraview|.
   -  Select **Tools → Start Trace**
   -  Open can.ex2.
   -  Turn all variables on.
   -  **Apply**.
   -  **+Y**.
   -  Go forward one timestep.
   -  Set **Coloring** to **EQPS**.
   -  Select **File → Save Screenshot**.
   -  **File → Save Animation**.
   -  **Tools → Stop trace**.
   -  Save this script on your desktop
   -  Edit the file and change the following:

      -  Correct the path to the input data and output screenshots or
         animations (not necessary, since you made the trace)
      -  Uncomment the line that says renderView*.ViewSize. Change this
         to something reasonable (maybe 1920x1080)

Let's now batch run this Python trace.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Delete the Screenshot and Animation you made above. We want to
   recreate these.
-  Open a **CMD** window. (On the Start button, type cmd, then click on
   Command Prompt.)
-  cd to the ParaView bin directory

   .. code-block:: console

      cd C:/Program Files (x86)/ParaView x.x.x/bin**

-  Use |pvpython| to process our trace. Notice that |pvpython| understands
   forward slashes.

   .. code-block:: console

      pvpython.exe C:/Users/myUserName/Desktop/trainingExampleScriptA.py

-  Open the Screenshot and Animation that you just made.

Let's edit the trace to accept arguments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Edit the python trace.
-  Right above the ExodusIIReader, enter the following code:

   .. code-block:: python

      datasetIn = sys.argv[1]
      directoryOut = sys.argv[2]
      imageFilesOut = sys.argv[3]
      print("datasetIn = " + datasetIn)
      print("directoryOut = " + directoryOut)
      print("imageFilesOut = " + imageFilesOut)

-  Edit the **canex2 = ExodusIIReader** line as follows:

   .. code-block:: python

      canex2 = ExodusIIReader(FileName=[datasetIn])

-  Edit the **SaveScreenshot(...)** line as follows:

   .. code-block:: python

      SaveScreenshot(directoryOut+imageFilesOut+'.png', renderView1,
      ImageResolution=[1425, 1324])

-  Edit the **SaveAnimation(...)** line as follows:

   .. code-block:: python

      SaveAnimation(directoryOut+imageFilesOut+'.avi', renderView1,
      ImageResolution=[1424, 1324], FrameWindow=[0, 43])

-  Now, run in a command window as follows:

   .. code-block:: console

      pvpython.exe "c:/Users/myUserName/Desktop/trainingExampleScriptA.py" "C:/Users/myUsername/Desktop" "coolVizA"

Generic Python Script for pvbatch on Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is an example running |pvbatch|, without having to make a trace.

-  Make a file greenSphere.py, as described in the |pvpython| section.
-  Copy greenSphere.py to be redSphere.py.
-  Edit redSphere.py

   -  Change **AmbientColor** and DiffuseColor to be [1.0, 0.0, 0.0]
   -  Change the output file from greenSphereScreenshot.png to
      redShpereScreenshotScreenshot.png

-  Make a file runner.sh
-  Edit as follows:

   .. code-block:: console

      #!/usr/bin/bash

      /pathTopvbatch/pvbatch greenSphere.py
      /pathTopvbatch/pvbatch redSphere.py

-  Run as follows:

   .. code-block:: console

      source runner.sh

Sandia National Labs specific section
-------------------------------------

This section is specific to the clusters and environment at Sandia
National Laboratories.

pvbatch on the clusters
~~~~~~~~~~~~~~~~~~~~~~~

ssh into one of the clusters. |pvbatch| can be run on the login nodes, and
magically will acquire compute nodes and run your batch visualization in
parallel. You will find test scripts at /projects/viz/training/paraview.
These scripts are run as follows:

.. code-block:: console

   /projects/viz/paraview/bin/pvbatch_chama_mesa

.. code-block:: console

   This is version x.x.x of |pvbatch|.
   Incorrect number of argument supplied. Expecting 4 but have 0

   Usage: /projects/viz/paraview/bin/pvbatch_chama_mesa <Nodes> <Minutes> <HERT estimate> batchFileFullPath

An example is:

.. code-block:: console

   /projects/viz/paraview/bin/pvbatch_chama_mesa 1 10 FY123456 /projects/viz/training/paraview/whipple-A.py

pvbatchOnNode on the clusters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|pvbatch| can be run on the same nodes as your simulation. Ask ParaView
help for more information on how to use this feature.

Example scripts
~~~~~~~~~~~~~~~

Here are four examples. We are going to create scripts using the trace
recorder, then run these scripts using |pvbatch|.

-  Read exodus data, screenshot, movie.

   -  Run |paraview|.
   -  **Tools → Start Trace**.
   -  Read g1s1.e.16.[0-15].
   -  Go to last timestep.
   -  Go back one timestep.
   -  Set **Coloring** to **EQPS**.
   -  **File → Save Screenshot**.
   -  **File → Save Animation**.
   -  **Tools → Stop trace**.
   -  Save this script on your cluster.
   -  Edit the file, change the following:

      -  Correct the path to the input data and output screenshots or
         animations.
      -  Uncomment the line that says **renderView*.ViewSize**. Change this
         to something reasonable (maybe 1920x1080).

-  Read exodus data, Clip, Slice, screenshot, movie.

   -  Run |paraview|.
   -  **Tools → Start Trace**.
   -  Read g1s1.e.16.[0-15].
   -  Go to last timestep.
   -  Go back one timestep.
   -  Set **Coloring** to **EQPS**.
   -  Select **Filters → Common → Clip**.
   -  Slice using **Y Normal**.
   -  **File → Save Screenshot**.
   -  **File → Save Animation**.
   -  **Tools → Stop trace**.
   -  Save this script on your cluster.
   -  Edit the file, change the following:

      -  Correct the path to the input data and output screenshots or
         animations
      -  Uncomment the line that says **renderView*.ViewSize**. Change this
         to something reasonable (maybe 1920x1080).
      -  Add the following line above **ColorBy(....,('EQPS'))**.

         .. code-block:: python

            g1s110fpse16Display.SetScalarBarVisibility(renderView1, False)

-  Read exodus data, 2d plots, screenshot, movie

   -  Run |paraview|.
   -  **Tools → Start Trace**.
   -  Read g1s1.e.16.[0-15].
   -  Select point if possible.
   -  Plot selection (or plot over line, if necessary).
   -  Plot EQPS.
   -  **File → Save Screenshot**.
   -  **File → Save Animation**.
   -  **Tools → Stop trace**.
   -  Edit the file, change the following:

      -  Correct the path to the input data and output screenshots or
         animations.
      -  Uncomment the line that says **renderView*.ViewSize**. Change this
         to something reasonable (maybe 1920x1080).

-  Read cth data, extractCTHPart, screenshot, movie.

   -  Run |paraview|.
   -  **Tools → Start Trace**.
   -  Read cth-med/spcth.[0-3].
   -  ExtractCTHParts - 1.
   -  ExtractCTHParts - 2.
   -  **Tools → Stop trace**.
